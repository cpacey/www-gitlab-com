---
layout: handbook-page-toc
title: "UX research at GitLab"
description: "The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals. We use these insights to inform and strengthen product and design decisions.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EQ750KX_6nU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

UX Researchers are one of the many GitLab Team Members who conduct user research. Other roles, such as Product Managers and Product Designers, frequently conduct research with guidance from the UX Research team.

Do you have questions about UX Research? The UX Research team is here for you! Reach out in the #ux_research Slack channel.

Below is a full list of our UX Research team handbook content:

### Conducting UX Research at GitLab

- [Defining goals, objectives, and hypotheses](/handbook/engineering/ux/ux-research-training/defining-goals-objectives-and-hypotheses/)
     - [How to write a strong hypothesis](/handbook/engineering/ux/ux-research-training/write-strong-hypothesis/)
- [Problem Validation and methods](/handbook/engineering/ux/ux-research-training/problem-validation-and-methods/)
     - [Problem Validation research for single-stage-group initiatives](/handbook/engineering/ux/ux-research-training/problem-validation-single-stage-group/)
     - [Choosing a research method for Problem Validation](/handbook/engineering/ux/ux-research-training/research-method-problem-validation/)
- [Solution Validation and methods](/handbook/engineering/ux/ux-research-training/solution-validation-and-methods/)
     - [Choosing a research method for Solution Validation](/handbook/engineering/ux/ux-research-training/research-method-solution-validation/)
- [Foundational research](/handbook/engineering/ux/ux-research-training/foundational-research/)
- [Strategic research at GitLab](/handbook/engineering/ux/ux-research-training/strategic-research-at-gitlab/)

### Research methods we use at GitLab

- [Choosing a methodology](/handbook/engineering/ux/ux-research-training/choosing-a-research-methodology/)
- [Longitudinal studies](/handbook/engineering/ux/ux-research-training/longitudinal-studies/)
- [Kano Model for feature prioritization](/handbook/engineering/ux/ux-research-training/kano-model/)
- [User story mapping](/handbook/engineering/ux/ux-research-training/user-story-mapping/)
- [Rapid Iterative Testing and Evaluation (RITE)](/handbook/engineering/ux/ux-research-training/rite/)
- [Usability at GitLab](/handbook/engineering/ux/ux-research-training/usability/)
     - [Usability testing](/handbook/engineering/ux/ux-research-training/usability-testing/)
     - [Unmoderated usability testing](/handbook/engineering/ux/ux-research-training/unmoderated-testing/)
     - [Usability benchmarking](/handbook/engineering/ux/ux-research-training/usability-benchmarking/)
     - [Writing a website usability testing script](/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/)
     - [UX Cloud Sandbox](/handbook/engineering/ux/ux-research-training/ux-cloud-sandbox/)
- [Facilitating user interviews](/handbook/engineering/ux/ux-research-training/facilitating-user-interviews/)
     - [Writing a discussion guide for user interviews](/handbook/engineering/ux/ux-research-training/discussion-guide-user-interviews/)
- [How to create a user persona](/handbook/engineering/ux/persona-creation/)
- [Evaluating navigation](/handbook/engineering/ux/ux-research-training/evaluating-navigation/)

### Finding participants

- [How to write an effective screener](/handbook/engineering/ux/ux-research-training/write-effective-screener/)
     - [The Common Screener: an efficient way to screen for multiple studies](/handbook/engineering/ux/ux-research-training/recruiting-participants/common-screener/)
- [Recruiting participants](/handbook/engineering/ux/ux-research-training/recruiting-participants/)

### Data and research insights

- [Collecting useful data](/handbook/engineering/ux/ux-research-training/collecting-useful-data/)
- [Using quantitative data to find insights](/handbook/engineering/ux/ux-research-training/quantitative-data/)
- [Analyzing and synthesizing user research data](/handbook/engineering/ux/ux-research-training/analyzing-research-data/)
- [Research insights](/handbook/engineering/ux/ux-research-training/research-insights/)
- [Documenting research insights in Dovetail](/handbook/engineering/ux/dovetail/)

### UX Research training

- [Interview Carousel - Becoming a better interviewer 15 minutes at a time](/handbook/engineering/ux/ux-research-training/interview-carousel/)
- [Creating design evaluations in Qualtrics](/handbook/engineering/ux/ux-research-training/creating-design-evaluations/)
     - [Qualtrics tips and tricks](/handbook/engineering/ux/qualtrics/)
- [UX Research shadowing](/handbook/engineering/ux/ux-research-training/research-shadowing/)

### Resources for UX Researchers

- [How the UX Research team operates at GitLab](/handbook/engineering/ux/ux-research-training/how-uxr-team-operates/)
- [Research prioritization](/handbook/engineering/ux/ux-research-training/research-prioritization/)
- [The IP Assignment and when to show it](/handbook/engineering/ux/ux-research-coordination/IP-Assignment/)
- [How to fill in for a UX Research Operations Coordinator](/handbook/engineering/ux/ux-research-coordination/research-coordinator-fill-in/)
- [UX Research growth and development](/handbook/engineering/ux/ux-research-training/ux-research-growth-and-development/)

### Resources for UX Research Operations Coordinators

- [UXR Operations Coordination at GitLab](/handbook/engineering/ux/ux-research-coordination/)
- [First Look UX Research panel](/handbook/engineering/ux/ux-research-coordination/first-look-ux-research-panel/)
- [Finding SaaS users](/handbook/engineering/ux/ux-research-training/finding-saas-users/)

### Measures and processes the UX Research team is responsible for establishing

- [System Usability Scale](/handbook/engineering/ux/performance-indicators/system-usability-scale/)
     - [System Usability Scale responder outreach](/handbook/engineering/ux/performance-indicators/system-usability-scale/sus-outreach.html)
     - [System Usability Scale database](/handbook/engineering/ux/sus-database/)
- [Category Maturity Scorecards](/handbook/engineering/ux/category-maturity-scorecards/)

